import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Account from './views/Account.vue'
import Register from './views/Register.vue'
import Resetpassword from './views/Resetpassword.vue'
import Resetpasswordcode from './views/Resetpasswordcode.vue'
import Confirmemail from './views/Confirmemail.vue'
import Product from './views/Product.vue'
import Orders from './views/Orders.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/account',
      name: 'account',
      component: Account
    },
    {
      path: '/account/register',
      name: 'register',
      component: Register
    },
    {
      path: '/account/resetpassword',
      name: 'resetpassword',
      component: Resetpassword
    },
    {
      path: '/account/resetpassword/:useremail/:code',
      name: 'resetpasswordcode',
      component: Resetpasswordcode,
      props: true 
    },
    {
      path: '/account/confirmemail/:userid/:code',
      name: 'confirmemail',
      component: Confirmemail,
      props: true 
    },
    {
      path: '/product/:productguid',
      name: 'product',
      component: Product,
      props: true 
    },
    {
      path: '/orders',
      name: 'orders',
      component: Orders,
      props: true 
    }
    
  ]
})
