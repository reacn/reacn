import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueSpinners from 'vue-spinners'
import VueSession from 'vue-session'
import VueJwtDecode from 'vue-jwt-decode'

var options = {
  persist: true
}

Vue.use(VueAxios, axios)
Vue.config.productionTip = false
Vue.use(VueAxios, axios)
Vue.use(VueSpinners)
Vue.use(VueSession, options)
Vue.use(VueJwtDecode)

//Vue.config.devtools=false 

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')



